import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="snips-plenae", # Yes, seriously, I think other people should be allowed to have their own snips pkg :-)
    version="0.0.2",
    author="Pieter Lenaerts",
    author_email="pieter.aj.lenaerts@gmail.com",
    description="Small python snippets for reuse",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://codeberg.org/plenae/python-snips",
    license="GPLv3+",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Operating System :: OS Independent",
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
    ],
    python_requires='>=3',
)
