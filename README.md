# python-snips

Small python snippets I want to reuse in other projects.

Feel free to reuse.

Don't assume anything is tested.

If you reuse, please post issues and improve if/when you find bugs.

## Installation
The commands below create a venv for your project "Pets" and installs
python-snips.

```
python -m venv Pets
source Pets/bin/activate
pip install wheel
pip install git+https://codeberg.org/plenae/python-snips.git

```
Check that the package is found on your path:
```
$ python -m snips.test
snips.test module loaded fine.
```
## Modules in package snips
### snips.argparse_actions
Contains argparse actions which I frequently need:
* DirType: checks if a dir is accessible in the requested mode.
* FileNewlineType: FileType but with the newline kwarg to open(). I use it to
  open CSV's.
### snips.logging
Contains logging configuration.
1. Use add_logging_args() on an ArgumentParser to
add a verbosity flag and a logfile argument to your CLI application.
2. Use config_logging() using verbosity and logfile to call
logging.basicConfig() with those arguments translated.
