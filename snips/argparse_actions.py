from argparse import ArgumentTypeError
import sys as _sys
import os
import os.path


class DirType(object):
    """Factory for creating directory object types

    Instances of DirType are typically passed as type= arguments to the
    ArgumentParser add_argument() method.

    Keyword Arguments:
        - mode -- A string indicating to test if the directory should be
            readable or writable as well. 'r' or 'w'.
    """

    def __init__(self, mode='r'):
        if mode not in ('r', 'w'):
            msg = 'mode should be "r" or "w", got %r' % mode
            raise ValueError(msg)
        self._mode = mode

    def __call__(self, string):
        if self._mode == 'r':
            if os.path.isdir(string):
                if os.access(string, os.R_OK):
                    return string
                else:
                    args = {'dirname': string}
                    message = "'%(dirname)s' not readable"
                    raise ArgumentTypeError(message % args)
            else:
                args = {'dirname': string}
                message = "'%(dirname)s' does not exist"
                raise ArgumentTypeError(message % args)

        if self._mode == 'w':
            if os.path.exists(string):
                if os.access(string, os.W_OK):
                    return string
                else:
                    args = {'dirname': string}
                    message = "'%(dirname)s' not writable"
                    raise ArgumentTypeError(message % args)
            else:
                try:
                    os.mkdir(string)
                    return string
                except OSError as e:
                    args = {'dirname': string, 'error': e}
                    message = "can't create '%(dirname)s': %(error)s"
                    raise ArgumentTypeError(message % args)

    def __repr__(self):
        return '%s(%s)' % (type(self).__name__, repr(self._mode))


class FileNewlineType(object):
    """Factory for creating file object types with newline argument

    Instances of FileType are typically passed as type= arguments to the
    ArgumentParser add_argument() method.

    Keyword Arguments:
        - mode -- A string indicating how the file is to be opened. Accepts the
            same values as the builtin open() function.
        - bufsize -- The file's desired buffer size. Accepts the same values as
            the builtin open() function.
        - encoding -- The file's encoding. Accepts the same values as the
            builtin open() function.
        - errors -- A string indicating how encoding and decoding errors are to
            be handled. Accepts the same value as the builtin open() function.
        - newline -- controls how universal newlines mode works (it only
            applies to text mode). It can be None, '', '\n', '\r', and '\r\n'.
    """

    def __init__(self, mode='r', bufsize=-1, encoding=None, errors=None,
                 newline=None):
        self._mode = mode
        self._bufsize = bufsize
        self._encoding = encoding
        self._errors = errors
        self._newline = newline

    def __call__(self, string):
        # the special argument "-" means sys.std{in,out}
        if string == '-':
            if 'r' in self._mode:
                return _sys.stdin
            elif 'w' in self._mode:
                return _sys.stdout
            else:
                msg = 'argument "-" with mode %r' % self._mode
                raise ValueError(msg)

        # all other arguments are used as file names
        try:
            return open(string, self._mode, self._bufsize, self._encoding,
                        self._errors, self._newline)
        except OSError as e:
            args = {'filename': string, 'error': e}
            message = "can't open '%(filename)s': %(error)s"
            raise ArgumentTypeError(message % args)

    def __repr__(self):
        args = self._mode, self._bufsize
        kwargs = [('encoding', self._encoding), ('errors', self._errors),
                  ('newline', self._newline)]
        args_str = ', '.join([repr(arg) for arg in args if arg != -1] +
                             ['%s=%r' % (kw, arg) for kw, arg in kwargs
                              if arg is not None])
        return '%s(%s)' % (type(self).__name__, args_str)
