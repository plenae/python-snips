import argparse
import sys
import logging
import io


def add_logging_args(parser):
    """Add logging arguments to an ArgumentParser.

    Verbose flag counts, logfile defaults to sys.stdout and appends in UTF-8 encoding.

    Use with snips.logging.config_logging(level, logfile)

    Arguments:
    - parser: the ArgumentParser object to add the
        """
    if not isinstance(parser, argparse.ArgumentParser):
        raise ValueError('parser argument must be of type argparse.ArgumentParser. Got ' + type(parser))
    parser.add_argument('--verbose', '-v', action='count', default=0,
                        help='Increase verbosity: -v = info, -vv = debug. ' +
                             'Default is warning. Logging goes to stderr.')
    parser.add_argument('--logfile', '-l', type=argparse.FileType('a', encoding='UTF-8'), nargs=1,
                        help='log file to write to. Default stderr.', default=sys.stderr)
    return parser


def get_log_level(level):
    """Return a logging.LEVEL based on the count of -v arguments.

    0 = WARNING, 1 = INFO, 2 = DEBUG.
    """
    if level == 0:
        return logging.WARNING
    elif level == 1:
        return logging.INFO
    elif level > 1:
        return logging.DEBUG


def config_logging(level, logfile):
    """Initiate basic logging configuration with the level requested on CLI.

    Arguments:
    - level: count of verbosity flags as with the argument added by snips.logging.add_logging_args().
             Ref snips.get_log_level().
    - logfile: logfile to append to. Can be a filename or an opened io.TextIOWrapper as returned by builtin open().
    """

    # Let logging.basicConfig open the file.
    if logfile == sys.stderr:
        logging.basicConfig(level=get_log_level(level))
    else:
        # Getting the file from argparse with nargs=1 results in a list. We need index 0
        if isinstance(logfile, list):
            filename = logfile[0]
        # If filename contains a file object, then refer to its name.
        if isinstance(filename, io.TextIOWrapper):
            filename = filename.name
        logging.basicConfig(level=get_log_level(level), filename=filename)

    logging.info('Running!')
    logging.debug('Loglevel is debug! Lots of info might be coming on stderr!')
